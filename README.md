# Anthropic chat app thingee

Chat app UI thing for Claude.

### Features

- Multiple conversation management
- Customizable system prompts (app, user, and conversation levels)
- Repository state synchronization options (full, changes, experimental, or none)
- Preview repo state which will be sent
- Message editing and deletion
  - Messages need to be explicitly added to history, for easier iterating of prompts 
- Streaming markdown rendering and code syntax highlighting
- Persistent storage of conversations

#### Experimental auto context
- Supplies automatic repo summary, such as
  - tooling used (and relevant parts of config files, such as prettier code formatting rules)
  - file tree (includes names of all exported symbols for each file (JS/TS only))
- Adds tool-calling functionality
  - `filesReaderTool` allows the model to request the full contents of files it thinks are relevant
  - `workingChangesTool` can get a git diff (staged/unstaged) to show your work in progress
  - `codeSearchTool` uses bundled [ripgrep](https://github.com/BurntSushi/ripgrep) for fast general searches over the codebase
  - ... more coming

![Example prompt showing tool use](docs/img/tool-use-example.png)

## Getting started

-   Clone this repo
-   Copy `example.env` as `.env` in the repo root, and add your Anthropic API key
-   Install [bun](https://bun.sh) if you're not already living on the bleeding edge
-   Run `bun install`

-   Start the project with `bun start`

## Notes

Repo state sync feature will send all matching code to Anthropic as part of each message. Be sure you don't have sensitive data in your code. There is no secret redaction here.

There is no limit on how many tokens this will try to use, so it's possible (easy) to send way too much data and hit token limits.

Conversation data is stored in the browsers LocalStorage.

## Screenshots

![Overview of conversation UI](docs/img/overview.png)

![Repo state sync settings](docs/img/repo-state-sync.png)

![Repo state preview](docs/img/repo-state-sync-preview.png)

![Cascading system prompts](docs/img/system-prompts.png)

## Development

Start client and server dev scripts in separate terminals:

```sh
bun dev:client
bun dev:server
```

## TODOs / ideas

-   calculate token usage from repo state up-front
-   implement request cancellation
-   handle bug when switching conversations while a response is still streaming in
