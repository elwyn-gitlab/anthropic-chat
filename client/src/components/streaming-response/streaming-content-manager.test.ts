import { describe, it, expect, beforeEach,  } from 'bun:test';
import { MarkdownContentChunk, StreamingContentManager } from './streaming-content-manager';

describe('StreamingContentManager', () => {
	let manager: StreamingContentManager;

	beforeEach(() => {
		manager = new StreamingContentManager();
	});

	describe('Basic Functionality', () => {
		it('should handle empty chunks', () => {
			manager.addChunk('');
			expect(manager.getCompletedBlocks()).toEqual([]);
			expect(manager.getCurrentBlock()).toEqual({
				text: '',
				complete: false,
				type: 'text',
			});
		});

		it('should accumulate chunks if no chunk boundary is detected', () => {
			manager.addChunk('Here is some ');
			manager.addChunk('simple text');
			expect(manager.getCompletedBlocks()).toEqual([]);
			expect(manager.getCurrentBlock()).toEqual({
				text: 'Here is some simple text',
				complete: false,
				type: 'text'
			});

			manager.addChunk('.\nAnd here is');
			manager.addChunk(' more text');
			expect(manager.getCompletedBlocks()).toEqual([]);
			expect(manager.getCurrentBlock()).toEqual({
				text: 'Here is some simple text.\nAnd here is more text',
				complete: false,
				type: 'text'
			});
		});

		describe('Basic markdown', () => {
			it('should not split basic markdown', () => {
				manager.addChunk('This has some ');
				manager.addChunk('**bo');
				manager.addChunk('ld** and *ita');
				manager.addChunk('lic* text');

				expect(manager.getCompletedBlocks()).toEqual([]);
				expect(manager.getCurrentBlock()).toEqual({
					text: 'This has some **bold** and *italic* text',
					complete: false,
					type: 'text'
				});
			});

			it('should handle links split across chunks', () => {
				manager.addChunk('Here is [a lin');
				manager.addChunk('k](https://exam');
				manager.addChunk('ple.com)');

				expect(manager.getCompletedBlocks()).toEqual([]);
				expect(manager.getCurrentBlock()).toEqual({
					text: 'Here is [a link](https://example.com)',
					complete: false,
					type: 'text'
				});
			});
		});
	});

	describe('Block boundaries', () => {
		describe('Paragraphs', () => {
			it('should split chunks into blocks when there is a double newline', () => {
				manager.addChunk('Here is some');
				manager.addChunk(' text.\n\n');
				manager.addChunk('And some');
				manager.addChunk(' more text');

				expect(manager.getCompletedBlocks()).toEqual([
					{
						text: 'Here is some text.\n\n',
						complete: true,
						type: 'text'
					} as MarkdownContentChunk,
				]);
				expect(manager.getCurrentBlock()).toEqual({
					text: 'And some more text',
					complete: false,
					type: 'text'
				});
			});

			it('should split multiple paragraphs', () => {
				manager.addChunk('Here is some');
				manager.addChunk(' text.\n\n');
				manager.addChunk('And some');
				manager.addChunk(' more text.\n\n');
				manager.addChunk('Even more');
				manager.addChunk(' text.\n\n');
				manager.addChunk('And wow');
				manager.addChunk(' more text.\n\n');

				expect(manager.getCompletedBlocks()).toEqual([
					{
						text: 'Here is some text.\n\n',
						complete: true,
						type: 'text'
					} as MarkdownContentChunk,
					{
						text: 'And some more text.\n\n',
						complete: true,
						type: 'text'
					} as MarkdownContentChunk,
					{
						text: 'Even more text.\n\n',
						complete: true,
						type: 'text'
					} as MarkdownContentChunk,
					{
						text: 'And wow more text.\n\n',
						complete: true,
						type: 'text'
					} as MarkdownContentChunk,
				]);
				expect(manager.getCurrentBlock()).toEqual({
					text: '',
					complete: false,
					type: 'text'
				});
			});

			it('should split chunks when block separator is mid chunk', () => {
				manager.addChunk('Here is some');
				manager.addChunk(' text.\n\nAnd');
				manager.addChunk(' some more text');

				expect(manager.getCompletedBlocks()).toEqual([
					{
						text: 'Here is some text.\n\n',
						complete: true,
						type: 'text'
					} as MarkdownContentChunk,
				]);
				expect(manager.getCurrentBlock()).toEqual({
					text: 'And some more text',
					complete: false,
					type: 'text'
				});
			});

			it('should split chunks with other basic markdown', () => {
				manager.addChunk('Here is some');
				manager.addChunk(' text.\n# Heading\nAnd');
				manager.addChunk(' some more text\n\nThen');
				manager.addChunk(' more');

				expect(manager.getCompletedBlocks()).toEqual([
					{
						text: 'Here is some text.\n# Heading\nAnd some more text\n\n',
						complete: true,
						type: 'text'
					} as MarkdownContentChunk,
				]);
				expect(manager.getCurrentBlock()).toEqual({
					text: 'Then more',
					complete: false,
					type: 'text'
				});
			});
		});
	});

	describe('Code Blocks', () => {
		it('should split a simple code block', () => {
			manager.addChunk('```\nf');
			manager.addChunk('oo\n`');
			manager.addChunk('``');

			expect(manager.getCompletedBlocks()).toEqual([
				{
					text: '```\nfoo\n```',
					complete: true,
					type: 'text'
				} as MarkdownContentChunk,
			]);
			expect(manager.getCurrentBlock()).toEqual({
				text: '',
				complete: false,
				type: 'text'
			});
		});

		it('should split a simple code block from a single chunk', () => {
			manager.addChunk('```\nfoo\n```\nbar');

			expect(manager.getCompletedBlocks()).toEqual([
				{
					text: '```\nfoo\n```',
					complete: true,
					type: 'text'
				} as MarkdownContentChunk,
			]);
			expect(manager.getCurrentBlock()).toEqual({
				text: '\nbar',
				complete: false,
				type: 'text'
			});
		});

		it('should split multiple simple code blocks', () => {
			manager.addChunk('```\nf');
			manager.addChunk('oo\n`');
			manager.addChunk('``\n```\nbar\n`');
			manager.addChunk('``\n');
			manager.addChunk('```\nbaz\n`');
			manager.addChunk('``\n');
			manager.addChunk('qux');

			expect(manager.getCompletedBlocks()).toEqual([
				{
					text: '```\nfoo\n```',
					complete: true,
					type: 'text'
				} as MarkdownContentChunk,
				{
					text: '\n',
					complete: true,
					type: 'text'
				} as MarkdownContentChunk,
				{
					text: '```\nbar\n```',
					complete: true,
					type: 'text'
				} as MarkdownContentChunk,
				{
					text: '\n',
					complete: true,
					type: 'text'
				} as MarkdownContentChunk,
				{
					text: '```\nbaz\n```',
					complete: true,
					type: 'text'
				} as MarkdownContentChunk,
			]);
			expect(manager.getCurrentBlock()).toEqual({
				text: '\nqux',
				complete: false,
				type: 'text'
			});
		});

		it('should handle code blocks with multi line content', () => {
			const chunks = [
				"Here's a code example:\n\n",
				'```typescript\n',
				'function test() {\n',
				'  console.log("hello");\n',
				'}\n',
				'```'
			];

			chunks.forEach(chunk => manager.addChunk(chunk));

			expect(manager.getCompletedBlocks()).toEqual([
				{
					text: "Here's a code example:\n\n",
					complete: true,
					type: 'text'
				} as MarkdownContentChunk,
				{
					text: '```typescript\nfunction test() {\n  console.log("hello");\n}\n```',
					complete: true,
					type: 'text'
				} as MarkdownContentChunk
			]);
			expect(manager.getCurrentBlock()).toEqual({
				text: '',
				complete: false,
				type: 'text'
			});
		});

		it('should handle code blocks with \n\n inside', () => {
			const chunks = [
				"Here's a code example:\n\n",
				'```typescript\n',
				'function test() {\n',
				'  console.log("hello");\n',
				'}\n\n',
				'const a = "wow";\n',
				'```'
			];

			chunks.forEach(chunk => manager.addChunk(chunk));

			expect(manager.getCompletedBlocks()).toEqual([
				{
					text: "Here's a code example:\n\n",
					complete: true,
					type: 'text'
				} as MarkdownContentChunk,
				{
					text: '```typescript\nfunction test() {\n  console.log("hello");\n}\n\nconst a = "wow";\n```',
					complete: true,
					type: 'text'
				} as MarkdownContentChunk
			]);
			expect(manager.getCurrentBlock()).toEqual({
				text: '',
				complete: false,
				type: 'text'
			});
		});

		it('should handle empty code blocks', () => {
			manager.addChunk('```\n```');

			expect(manager.getCompletedBlocks()).toEqual([{
				text: '```\n```',
				complete: true,
				type: 'text'
			} as MarkdownContentChunk]);
		});

		it('should handle split language identifiers', () => {
			manager.addChunk('```type');
			manager.addChunk('script\nfoo\n```');

			expect(manager.getCompletedBlocks()).toEqual([{
				text: '```typescript\nfoo\n```',
				complete: true,
				type: 'text'
			} as MarkdownContentChunk]);
		});

		it('should not complete partial code blocks', () => {
			manager.addChunk('```typescript\nfoo\n``');  // Missing one backtick

			expect(manager.getCompletedBlocks()).toEqual([]);
			expect(manager.getCurrentBlock()).toEqual({
				text: '```typescript\nfoo\n``',
				complete: false,
				type: 'text'
			});
		});

		it('should handle nested backticks in code blocks', () => {
			manager.addChunk('```\nThis has `inline` code and');
			manager.addChunk(' ``double`` backticks\n```');

			expect(manager.getCompletedBlocks()).toEqual([{
				text: '```\nThis has `inline` code and ``double`` backticks\n```',
				complete: true,
				type: 'text'
			} as MarkdownContentChunk]);
		});

		it.skip('should handle code blocks with nested triple backticks', () => {
			// TODO: this edge case is not handled.

			manager.addChunk('```typescript\n');
			manager.addChunk('const foo = "');
			manager.addChunk('```');  // These are the actual triple backticks we want to preserve
			manager.addChunk('";\n');
			manager.addChunk('```');  // This is the closing marker

			expect(manager.getCompletedBlocks()).toEqual([{
				text: '```typescript\nconst foo = "```";\n```',
				complete: true,
				type: 'text'
			} as MarkdownContentChunk]);
		});
	});

	describe('Thinking blocks', () => {
		it('should split a simple code block', () => {
			manager.addChunk('<thinking>\nf');
			manager.addChunk('oo\n</thi');
			manager.addChunk('nking>');

			expect(manager.getCompletedBlocks()).toEqual([
				{
					text: '<thinking>\nfoo\n</thinking>',
					complete: true,
					type: 'text'
				} as MarkdownContentChunk,
			]);
			expect(manager.getCurrentBlock()).toEqual({
				text: '',
				complete: false,
				type: 'text'
			});
		});
	});

	describe('handleContentBlock', () => {
		it('should remove matching completed blocks when receiving a finalised contentBlock from the backend', () => {
			const manager = new StreamingContentManager();

			// Add some content that will be split
			manager.addChunk('Hello');
			manager.addChunk(' world');
			manager.addChunk('\n\n');

			// Add a content block that matches
			manager.handleContentBlock({ type: 'text', text: 'Hello world' });

			// Check that matching blocks were removed
			expect(manager.getCompletedBlocks()).toHaveLength(0);
		});
	})
});
