import Anthropic from '@anthropic-ai/sdk';

export interface ContentChunk {
	complete: boolean;
	type: 'text' | 'tool_use'
}

export interface MarkdownContentChunk extends ContentChunk, Anthropic.TextBlock {
	type: 'text';
	text: string;
}

export interface ToolContentChunk extends ContentChunk, Anthropic.ToolUseBlock {
	type: 'tool_use';
}

export class StreamingContentManager {
	private buffer = '';
	private completedBlocks: Array<ContentChunk> = [];
	private isInCodeBlock = false;
	private isInThinkingBlock = false;

	addChunk(chunk: string): void {
		this.buffer += chunk;
		this.processBuffer();
	}

	private processBuffer(): void {
		let foundSeparator: boolean;
		do {
			foundSeparator = false;

			if (this.isInCodeBlock) {
				foundSeparator = this.processCodeBlockEnd();
			} else if(this.isInThinkingBlock) {
				foundSeparator = this.processThinkingBlockEnd();
			} else {
				foundSeparator = this.processRegularContent();
			}
		} while (foundSeparator);
	}

	private processCodeBlockEnd(): boolean {
		const endMarkerIndex = this.buffer.indexOf('```', 1);
		if (endMarkerIndex !== -1) {
			this.extractBlock(endMarkerIndex + 3, true, 'text');
			this.isInCodeBlock = false;
			return true;
		}
		return false;
	}

	private processThinkingBlockEnd(): boolean {
		const endMarkerIndex = this.buffer.indexOf('</thinking>');
		if (endMarkerIndex !== -1) {
			this.extractBlock(endMarkerIndex + 11, true, 'text');
			this.isInThinkingBlock = false;
			return true;
		}
		return false;
	}

	private processRegularContent(): boolean {
		const codeBlockStart = this.buffer.indexOf('```');
		if (codeBlockStart !== -1) {
			if (codeBlockStart > 0) {
				this.extractBlock(codeBlockStart, true, 'text');
			}
			this.isInCodeBlock = true;
			return true;
		}

		const thinkingStart = this.buffer.indexOf('<thinking>');
		if (thinkingStart !== -1) {
			if (thinkingStart > 0) {
				this.extractBlock(thinkingStart, true, 'text');
			}
			this.isInThinkingBlock = true;
			return true;
		}

		const paragraphEnd = this.buffer.indexOf('\n\n');
		if (paragraphEnd !== -1) {
			this.extractBlock(paragraphEnd + 2, true, 'text');
			return true;
		}

		return false;
	}

	private extractBlock(endIndex: number, complete: boolean, type: ContentChunk['type']): void {
		const block = this.buffer.slice(0, endIndex);
		this.completedBlocks.push({ text: block, complete, type: type } as MarkdownContentChunk);
		this.buffer = this.buffer.slice(endIndex);
	}

	getCompletedBlocks(): Array<ContentChunk> {
		return this.completedBlocks;
	}

	getCurrentBlock(): MarkdownContentChunk {
		return {
			type: 'text',
			text: this.buffer,
			complete: false,
		};
	}

	handleContentBlock(contentBlock: Anthropic.ContentBlock) {
		if (contentBlock.type !== 'text' || !contentBlock.text) return;

		console.log('Processing content block:', {
			text: contentBlock.text,
			completedBlocks: this.completedBlocks,
			currentBuffer: this.buffer
		});

		let remainingText = contentBlock.text;

		// Process completed blocks in reverse order (newest first)
		// This helps with matching nested content more accurately
		for (let i = this.completedBlocks.length - 1; i >= 0; i--) {
			const block = this.completedBlocks[i] as MarkdownContentChunk;

			if (!('text' in block) || !block.text) continue;

			// Normalize whitespace and newlines for comparison
			const normalizedBlockText = block.text.replace(/\s+/g, ' ').trim();
			const normalizedRemainingText = remainingText.replace(/\s+/g, ' ').trim();

			if (normalizedRemainingText.includes(normalizedBlockText)) {
				// Remove the block since we found it in the content
				this.completedBlocks.splice(i, 1);

				// Don't update remainingText since blocks might not be in order
				// Just mark this one as processed
			}
		}

		// Also check the current buffer if it's not empty
		if (this.buffer) {
			const normalizedBuffer = this.buffer.replace(/\s+/g, ' ').trim();
			const normalizedText = remainingText.replace(/\s+/g, ' ').trim();

			if (normalizedText.includes(normalizedBuffer)) {
				this.buffer = '';
			}
		}

		console.log('After processing:', {
			remainingCompletedBlocks: this.completedBlocks,
			remainingBuffer: this.buffer
		});
	}

	finalizeResponse() {
		this.buffer = '';
		// this.completedBlocks = [];
	}
}
