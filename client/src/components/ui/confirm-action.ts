import { createApp, h } from 'vue';

import ConfirmModal from './ConfirmModal.vue';

interface ConfirmOptions {
	title: string;
	message: string;
	confirmButtonText?: string;
	confirmButtonVariant?: string;
	cancelButtonText?: string;
	cancelButtonVariant?: string;
}

export async function confirmAction(options: ConfirmOptions): Promise<boolean> {
	return new Promise((resolve) => {
		const app = createApp({
			render: () =>
				h(ConfirmModal, {
					visible: true,
					...options,
					onConfirm: () => {
						app.unmount();
						resolve(true);
					},
					onCancel: () => {
						app.unmount();
						resolve(false);
					},
				}),
		});

		const container = document.createElement('div');
		document.body.prepend(container);
		app.mount(container);
	});
}
