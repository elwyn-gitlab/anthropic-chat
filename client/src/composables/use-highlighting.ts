import { uid } from 'radash';
import { codeToHtml, bundledLanguages, bundledLanguagesInfo } from 'shiki';
import { logger } from '../logger.ts';

const COPY_CODE_BLOCK_CLASS = 'js-copy-code-block';
let hasGlobalCopyEventHandler = false;
const codeChunks = new Map<string, string>();

export function useHighlighting() {
	if (!hasGlobalCopyEventHandler) {
		document.addEventListener('click', async (event) => {
			const target = event.target as HTMLElement;
			const isCopyCodeBlockButton =
				target.classList.contains(COPY_CODE_BLOCK_CLASS) && target.id;
			if (!isCopyCodeBlockButton) {
				return;
			}

			const code = codeChunks.get(target.id);
			if (!code) {
				return;
			}

			event.stopPropagation();
			event.preventDefault();
			try {
				await navigator.clipboard.writeText(
					codeChunks.get(target.id) || '',
				);

				target.innerHTML = `<span class="text-green-600 mr-1">✓</span>copy`;
				setTimeout(() => {
					if (target) {
						target.innerHTML = 'copy';
					}
				}, 5000);
			} catch (err) {
				const errMsg = 'Failed to write to clipboard.';
				console.error(errMsg);
				console.error(err);
				alert(errMsg);
			}
		});

		hasGlobalCopyEventHandler = true;
	}

	async function highlight(code: string, lang: string) {
		const replacements: Record<string, string> = {
			'patch': 'diff',
			'text': ''
		};
		if (replacements[lang]) {
			lang = replacements[lang];
		}

		const isSupportedLang = Object.keys(bundledLanguages).includes(lang);

		let highlightedCode = '';
		try {
			if (isSupportedLang) {
				highlightedCode = await codeToHtml(code, {
					lang,
					theme: 'slack-dark',
				});
			} else {
				logger.warn(
					{ lang, code },
					`Skipped syntax highlighting code block for unsupported language "${lang}"`,
				);
			}
		} catch (error) {
			// format error, oh well, best effort
			logger.error(
				{ code, lang, error },
				`Failed to apply syntax highlighting to code block`,
			);
		} finally {
			highlightedCode =
				highlightedCode || `<pre><code>${code}</code></pre>`;
		}

		const id = uid(8);
		codeChunks.set(id, code);
		return `<button type="button" id="${id}" class="${COPY_CODE_BLOCK_CLASS} absolute top-0 right-3">copy</button>${highlightedCode}`;
	}

	async function getLanguageFromExtension(
		fileExtension: string,
	): Promise<string | null> {
		const ext = fileExtension.toLowerCase();
		const lang = bundledLanguagesInfo.find((l) => {
			if (l.id.toLowerCase() === ext || l.name.toLowerCase() === ext) {
				return true;
			}

			return l.aliases?.find((a) => a.toLowerCase() === ext) || false;
		});

		return lang?.id || null;
	}

	return {
		highlight,
		getLanguageFromExtension,
	};
}
