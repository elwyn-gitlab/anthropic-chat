import { Marked } from 'marked';
import { markedHighlight } from 'marked-highlight';
import { useHighlighting } from './use-highlighting.ts';

export function useMarkdown() {
	const { highlight } = useHighlighting();

	const marked = new Marked(
		markedHighlight({
			langPrefix: 'highlight not-prose relative language-',
			async: true,
			highlight,
		}),
	);
	marked.use({
		renderer: {
			code: (code) => `<div class="block relative">${code}</div>`,
		},
	});

	return {
		marked,
	};
}
