import { createApp } from 'vue';
import { createPinia } from 'pinia';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';

import './style.css';
import App from './App.vue';
import { connectSocket } from './ws';
import { useAppStore } from './store/app.store.ts';
import { useActiveConversationStore } from './store/active-conversation.store.ts';
import { logger } from './logger';
import { router } from './router.ts';

const app = createApp(App);
const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);

app.use(pinia);
app.use(router);

const appStore = useAppStore();
const activeConversationStore = useActiveConversationStore();
connectSocket(appStore, activeConversationStore).then(() => {
	app.mount('#app');
	logger.info('Vue app mounted');
});
