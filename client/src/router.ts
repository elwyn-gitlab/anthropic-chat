import { createRouter, createWebHistory } from 'vue-router';

import Home from './components/home/Home.vue';
import Conversation from './components/Conversation.vue';
import SystemPromptModal from './components/sidebar/SystemPromptModal.vue';
import RepoStateModal from './components/sidebar/RepoStateModal.vue';
import RepoStatePreview from './components/sidebar/RepoStatePreview.vue';

export const router = createRouter({
	history: createWebHistory(),
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home,
		},
		{
			path: '/conversation/:id',
			component: Conversation,
			children: [
				{
					path: '',
					name: 'conversation',
					component: { render: () => null },
				},
				{
					path: 'system-prompt',
					name: 'system-prompt',
					component: SystemPromptModal,
				},
				{
					path: 'repo-state',
					name: 'repo-state',
					component: RepoStateModal,
				},
				{
					path: 'repo-state/preview/:from?',
					name: 'repo-state-preview',
					component: RepoStatePreview,
				},
			],
		},
		{
			path: '/:catchAll(.*)',
			redirect: '/',
		},
	],
});
