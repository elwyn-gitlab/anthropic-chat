import { defineStore } from 'pinia';

import {
	AnthropicError,
	FileState,
	IConversation,
	MessagePair,
	useAppStore,
} from './app.store';
import { PromptMessage } from '../ws';
import { StreamingContentManager } from '../components/streaming-response/streaming-content-manager.ts';
import Anthropic from '@anthropic-ai/sdk';

export const useActiveConversationStore = defineStore('active-conversation', {
	state: () => ({
		streamingContentManager: new StreamingContentManager(),
	}),
	getters: {
		activeConversation(): IConversation {
			const appStore = useAppStore();
			return appStore.conversations.find(
				(conversation) =>
					conversation.id === appStore.activeConversationId,
			)!;
		},
	},
	actions: {
		addToHistory(response: string) {
			const messagePair: MessagePair = {
				user: this.activeConversation.prompt,
				assistant: response,
			};
			this.activeConversation.messages.push(messagePair);
			this.activeConversation.prompt = '';
		},
		removeFromHistory(message: MessagePair) {
			const idx = this.activeConversation.messages.findIndex(
				(m) =>
					m.user === message.user &&
					m.assistant === message.assistant,
			);
			if (idx === -1) {
				console.error(
					'Failed to find message to remove from history :( ',
					{
						message,
						activeConversationMessages:
							this.activeConversation.messages,
					},
				);
				return;
			}
			this.activeConversation.messages.splice(idx, 1);
		},
		clearHistory() {
			this.activeConversation.messages = [];
		},
		onText(text: string) {
			this.streamingContentManager.addChunk(text);
		},
		onContentBlock(contentBlock: Anthropic.ContentBlock) {
			console.log('Got content block:', { contentBlock });

			// Only process text blocks
			if (contentBlock.type !== 'text') {
				this.streamingContentManager.handleContentBlock(contentBlock);
				this.activeConversation.streamingResponse.contentBlocks.push(contentBlock);
				return;
			}

			const text = contentBlock.text;
			const blocks: Array<Anthropic.ContentBlock> = [];

			// Find all thinking sections with content before/after
			let lastIndex = 0;
			const thinkingRegex = /<thinking>([\s\S]*?)<\/thinking>/g;
			let match;

			while ((match = thinkingRegex.exec(text)) !== null) {
				// Add text before thinking block if exists
				if (match.index > lastIndex) {
					const beforeText = text.slice(lastIndex, match.index).trim();
					if (beforeText) {
						blocks.push({
							type: 'text',
							text: beforeText
						});
					}
				}

				// Add thinking block with full tags
				const fullThinkingBlock = match[0].trim();
				if (fullThinkingBlock) {
					blocks.push({
						type: 'text',
						text: fullThinkingBlock
					});
				}

				lastIndex = match.index + match[0].length;
			}

			// Add remaining text after last thinking block if exists
			if (lastIndex < text.length) {
				const remainingText = text.slice(lastIndex).trim();
				if (remainingText) {
					blocks.push({
						type: 'text',
						text: remainingText
					});
				}
			}

			// If no thinking sections were found, treat as normal text block
			if (blocks.length === 0) {
				blocks.push(contentBlock);
			}

			// Process all blocks in order
			blocks.forEach(block => {
				this.streamingContentManager.handleContentBlock(block);
				this.activeConversation.streamingResponse.contentBlocks.push(block);
			});
		},
		onError(error: AnthropicError) {
			console.error('got error', error);
			this.activeConversation.streamingResponse.error = error;
			this.activeConversation.streamingResponse.loading = false;
		},
		onResponseComplete(finalResponse: Array<{ text: string }>) {
			this.activeConversation.streamingResponse.loading = false;
			if (finalResponse.length > 1) {
				alert(
					'Whoa! Response with more things than expected. Check console.',
				);
				console.error(
					'Code only written to handle a response with 1 item in it, but got more.',
				);
				console.error({ finalResponse });
			}
			this.activeConversation.streamingResponse.response =
				finalResponse.at(0)!.text;
			this.streamingContentManager.finalizeResponse();
		},
		resetStreamingContentManager() {
			this.streamingContentManager = new StreamingContentManager();
		},
		onClearResponse() {
			this.activeConversation.streamingResponse.loading = false;
			this.activeConversation.streamingResponse.response = '';
			this.activeConversation.streamingResponse.contentBlocks = [];
			this.resetStreamingContentManager();
		},
		async submitPrompt() {
			const { sendMessage, appSystemPrompt, userSystemPrompt } =
				useAppStore();

			this.activeConversation.streamingResponse.error = undefined;
			this.activeConversation.streamingResponse.loading = true;
			this.activeConversation.streamingResponse.response = '';
			this.activeConversation.streamingResponse.contentBlocks = [];
			this.resetStreamingContentManager();

			const {
				streamingResponse,
				repoStatePreview,
				systemPrompt,
				...conversation
			} = this.activeConversation;
			await sendMessage({
				type: 'prompt',
				value: {
					...conversation,
					systemPrompt: [
						appSystemPrompt,
						userSystemPrompt,
						systemPrompt,
					].join('\n'),
				} as PromptMessage,
			});
		},
		async getRepoStatePreview() {
			const { sendMessage } = useAppStore();

			const { repoState, repoPath, repoIgnoreList } =
				this.activeConversation;

			if (repoState === 'none') {
				return;
			}

			this.activeConversation.repoStatePreview.loading = true;
			await sendMessage({
				type: 'get-repo-state-preview',
				value: {
					repoState,
					repoPath,
					repoIgnoreList,
				},
			});
		},
		onRepoStatePreview(files: Array<FileState>) {
			this.activeConversation.repoStatePreview.loading = false;
			this.activeConversation.repoStatePreview.files = files;
		},
		getCompletedBlocks() {
			return this.streamingContentManager.getCompletedBlocks();
		},
		getCurrentBlock() {
			return this.streamingContentManager.getCurrentBlock();
		},
	},
});

export type ActiveConversationStore = ReturnType<
	typeof useActiveConversationStore
>;
