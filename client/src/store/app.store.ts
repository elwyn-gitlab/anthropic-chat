import { defineStore } from 'pinia';
import { uid } from 'radash';

import { MessageWrapper } from '../ws';
import { getDefaultAppPrompt } from './prompts/default-app-prompt.ts';
import { getDefaultUserPrompt } from './prompts/default-user-prompt.ts';
import { getDefaultConversationPrompt } from './prompts/default-conversation-prompt.ts';
import { useActiveConversationStore } from './active-conversation.store.ts';
import Anthropic from '@anthropic-ai/sdk';

export interface MessagePair {
	user: string;
	assistant: string;
}

type SendMessageHandler = <T>(message: MessageWrapper<T>) => Promise<void>;

export const useAppStore = defineStore('app', {
	persist: {
		paths: [
			'conversations',
			'leftPanelWidth',
			'promptInputHeight',
			'appSystemPrompt',
			'userSystemPrompt',
		],
	},
	state: () => ({
		activeConversationId: '',
		conversations: [getNewConversation()] as Array<IConversation>,
		sidebarExpanded: false,
		leftPanelWidth: null as number | null,
		promptInputHeight: 500,
		sendMessage: null as any as SendMessageHandler, // TODO ew
		agentConnected: false,
		appSystemPrompt: getDefaultAppPrompt(),
		userSystemPrompt: getDefaultUserPrompt(),
	}),
	actions: {
		isValidConversation(id: string) {
			return this.conversations.some(
				(conversation) => conversation.id === id,
			);
		},
		setActiveConversation(id: string) {
			if (this.isValidConversation(id)) {
				this.activeConversationId = id;
				const activeConversationStore = useActiveConversationStore();
				activeConversationStore.resetStreamingContentManager();
			}
		},
		addNewConversation() {
			const newConversation = getNewConversation();
			this.conversations.push(newConversation);
			this.activeConversationId = newConversation.id;
			document.getElementById('active-conversation-name-input')?.focus();
		},
		deleteConversation(id: string) {
			this.conversations = this.conversations.filter(
				(conversation) => conversation.id !== id,
			);
			if (this.conversations.length) {
				this.setActiveConversation(this.conversations.at(0)!.id);
			} else {
				this.addNewConversation();
			}
		},
		onConnectionChange(connected: boolean) {
			this.agentConnected = connected;
		},
	},
});

export type AppStore = ReturnType<typeof useAppStore>;

export function getNewConversation(): IConversation {
	const id = uid(8);
	return {
		id,
		title: id,
		systemPrompt: getDefaultConversationPrompt(),
		prompt: '',
		messages: [] as Array<MessagePair>,
		repoPath: undefined as string | undefined,
		repoState: 'none',
		repoIgnoreList: '.env,package-lock.json,yarn.lock,bun.lockb',
		streamingResponse: {
			loading: false,
			response: '',
			contentBlocks: []
		},
		repoStatePreview: {
			loading: false,
			files: [],
		},
	};
}

export interface IConversation {
	id: string;
	title: string;
	systemPrompt: string;
	prompt: string;
	messages: Array<MessagePair>;
	repoPath: string | undefined;
	repoState: 'none' | 'full' | 'changes' | 'experimental';
	repoIgnoreList: string;
	streamingResponse: {
		loading: boolean;
		response: string; // final response, TODO replace
		contentBlocks: Array<Anthropic.ContentBlock>;
		error?: AnthropicError;
	};
	repoStatePreview: {
		loading: boolean;
		files: Array<FileState>;
	};
}

export interface AnthropicError {
	status: unknown;
	headers: unknown;
	error: {
		type: string;
		error: {
			type: string;
			message: string;
		};
	};
}

export interface FileState {
	filePath: string;
	contents: string;
}
