declare module 'vue3-smooth-dnd' {
	import { DefineComponent } from 'vue';

	interface DropPlaceholder {
		className: string;
		animationDuration?: number;
		showOnTop?: boolean;
	}

	interface NodeDescription {
		value: string;
		props?: Record<string, any>;
	}

	interface DragResult {
		payload: any;
		isSource: boolean;
		willAcceptDrop: boolean;
	}

	interface DropResult {
		addedIndex: number | null;
		removedIndex: number | null;
		payload: any;
		element: HTMLElement;
	}

	export const Container: DefineComponent<{
		orientation?: 'horizontal' | 'vertical';
		behaviour?: 'move' | 'copy' | 'drop-zone' | 'contain';
		tag?: string | NodeDescription;
		groupName?: string;
		lockAxis?: 'x' | 'y';
		dragHandleSelector?: string;
		nonDragAreaSelector?: string;
		dragBeginDelay?: number;
		animationDuration?: number;
		autoScrollEnabled?: boolean;
		dragClass?: string;
		dropClass?: string;
		removeOnDropOut?: boolean;
		dropPlaceholder?: boolean | DropPlaceholder;
		getChildPayload?: (index: number) => any;
		shouldAcceptDrop?: (
			sourceContainerOptions: any,
			payload: any,
		) => boolean;
		shouldAnimateDrop?: (
			sourceContainerOptions: any,
			payload: any,
		) => boolean;
		getGhostParent?: () => HTMLElement;
		onDragStart?: (dragResult: DragResult) => void;
		onDragEnd?: (dragResult: DragResult) => void;
		onDragEnter?: () => void;
		onDragLeave?: () => void;
		onDropReady?: (dropResult: DropResult) => void;
		onDrop?: (dropResult: DropResult) => void;
	}>;

	export const Draggable: DefineComponent<{
		tag?: string | NodeDescription;
	}>;
}
