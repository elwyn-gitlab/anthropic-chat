import { AnthropicError, AppStore, FileState } from '../store/app.store';
import { IConversation } from '../store/app.store.ts';
import { ActiveConversationStore } from '../store/active-conversation.store.ts';
import { logger } from '../logger';
import Anthropic from '@anthropic-ai/sdk';

export interface MessageWrapper<T> {
	type: string;
	value: T;
}

export type PromptMessage = Omit<IConversation, 'streamingResponse'>;

const MAX_RETRY_DELAY = 10000;
const INITIAL_RETRY_DELAY = 1000;

export async function connectSocket(
	appStore: AppStore,
	activeConversationStore: ActiveConversationStore,
) {
	let wss: WebSocket;
	let retryCount = 0;
	let retryDelay = INITIAL_RETRY_DELAY;

	const { onConnectionChange } = appStore;
	const { onText, onContentBlock, onError, onResponseComplete, onRepoStatePreview } =
		activeConversationStore;

	async function sendMessage<T>(message: MessageWrapper<T>) {
		if (
			wss.readyState === WebSocket.CLOSING ||
			wss.readyState === WebSocket.CLOSED
		) {
			onConnectionChange(false);
			await connectWebSocket();
		}

		wss.send(JSON.stringify(message));
	}
	appStore.sendMessage = sendMessage;

	function connectWebSocket(): Promise<void> {
		return new Promise((resolve) => {
			function connect() {
				wss?.close();
				wss?.removeEventListener('message', onMessage);

				// TODO better sync of ws host,port between client/server
				wss = new WebSocket('ws://localhost:3939/chat');

				wss.addEventListener('open', () => {
					wss.addEventListener('message', onMessage);
					onConnectionChange(true);
					retryCount = 0;
					retryDelay = INITIAL_RETRY_DELAY;
					resolve();
				});

				wss.addEventListener('close', (event) => {
					onConnectionChange(false);

					if (!event.wasClean) {
						retryConnection();
					}
				});

				wss.addEventListener('error', () => {
					wss.close();
				});
			}

			function retryConnection() {
				retryCount++;
				const delay = Math.min(
					retryDelay * Math.pow(2, retryCount - 1),
					MAX_RETRY_DELAY,
				);
				logger.info(
					`WebSocket disconnected. Retrying in ${delay}ms...`,
				);
				setTimeout(connect, delay);
			}

			connect();
		});
	}

	// TODO proper generics
	function onMessage<T extends unknown>(event: MessageEvent) {
		const message: MessageWrapper<T> = JSON.parse(event.data);
		switch (message.type) {
			case 'chunk-delta':
				onText(message.value as string);
				break;
			case 'content-block':
				onContentBlock(message.value as Anthropic.ContentBlock);
				break;
			case 'response-error':
				onError(message.value as AnthropicError);
				break;
			case 'response-complete':
				onResponseComplete(message.value as Array<{ text: string }>);
				break;
			case 'repo-state-preview':
				onRepoStatePreview(message.value as Array<FileState>);
				break;
			default:
				logger.warn('Unknown message type:', message);
		}
	}

	await connectWebSocket();

	return { sendMessage };
}
