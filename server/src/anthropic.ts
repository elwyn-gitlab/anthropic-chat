import Anthropic, { AnthropicError } from '@anthropic-ai/sdk';
import { ServerWebSocket } from 'bun';

import { getRepoState } from './repo-state';
import { PromptMessage } from '../../client/src/ws';
import { logger } from './logger';
import { getExperimentalRepoStateMessage } from './experimental';
import { BaseToolInput, toolRegistry } from './experimental/tools';

// Log response streams as they come in
const NOISY_ANTHROPIC_LOGS = false;

export const client = new Anthropic({
	apiKey: process.env.ANTHROPIC_API_KEY,
});
export const MESSAGE_BASE: Partial<Anthropic.MessageStreamParams> = {
	model: process.env.ANTHROPIC_MODEL,
	max_tokens: 4096,
};

export async function runPrompt(
	promptArgs: PromptMessage,
	ws: ServerWebSocket<unknown>,
): Promise<void> {
	const messages = await createMessages(promptArgs);
	await executePromptWithTools(promptArgs, messages, ws);
}

async function executePromptWithTools(
	promptArgs: PromptMessage,
	messages: Array<Anthropic.MessageParam>,
	ws: ServerWebSocket<unknown>,
): Promise<void> {
	const { systemPrompt, repoState, repoPath, repoIgnoreList } = promptArgs;
	const ignoreList = repoIgnoreList.split(',').map((item) => item.trim());

	const anthropicOptions = {
		...MESSAGE_BASE,
		system: systemPrompt,
		messages,
	} as Anthropic.MessageStreamParams;

	if (repoState === 'experimental') {
		anthropicOptions.tools = toolRegistry.getRegisteredTools();
		anthropicOptions.tool_choice = {
			type: 'auto',
			disable_parallel_tool_use: false,
		};
	}

	try {
		const stream = client.messages
			.stream(anthropicOptions)
			.on('text', (delta) => {
				NOISY_ANTHROPIC_LOGS && logger.debug('on text\n', JSON.stringify(delta, null, 4));

				ws.send(
					JSON.stringify({
						type: 'chunk-delta',
						value: delta,
					}),
				);
			})
			.on('contentBlock', (contentBlock) => {
				NOISY_ANTHROPIC_LOGS && logger.debug(
					'on contentBlock\n',
					JSON.stringify(contentBlock, null, 4),
				);
				ws.send(
					JSON.stringify({
						type: 'content-block',
						value: contentBlock,
					}),
				);
			})
			.on('error', (err) => {
				onAnthropicError(err, ws);
			});

		const response = await stream.finalMessage();

		// If Claude wants to use a tool
		if (response.stop_reason === 'tool_use') {
			const toolUse = response.content.find(
				(content): content is Anthropic.ToolUseBlock =>
					content.type === 'tool_use',
			);

			if (!toolUse) {
				throw new Error('Tool use response missing tool_use block');
			}

			const tool = toolRegistry.getTool(toolUse.name);
			if (!tool) {
				throw new Error(`Unknown tool: ${toolUse.name}`);
			}

			try {
				const result = await tool.execute({
					...toolUse.input as BaseToolInput,
					repoPath,
					repoIgnoreList: ignoreList,
				});

				const updatedMessages: Array<Anthropic.MessageParam> = [
					...messages,
					{ role: response.role, content: response.content },
					{
						role: 'user',
						content: [
							{
								type: 'tool_result',
								tool_use_id: toolUse.id,
								content: result,
							},
						],
					},
				];

				return executePromptWithTools(promptArgs, updatedMessages, ws);
			} catch (error) {
				logger.error({ error }, 'Tool execution failed');
				throw error;
			}
		}

		logger.debug('Response complete\n', JSON.stringify(response, null, 4));
		ws.send(
			JSON.stringify({
				type: 'response-complete',
				value: response.content,
			}),
		);
	} catch (error) {
		onAnthropicError(error, ws);
	}
}

function onAnthropicError(error: AnthropicError, ws: ServerWebSocket<unknown>) {
	let err: any = error;
	if (
		err instanceof Error &&
		'status' in err &&
		err.status === undefined &&
		'cause' in err &&
		err.cause
	) {
		err = err.cause;
	}

	if(err.message.startsWith('SSE Error: {')) {
		try {
			err = JSON.parse(err.message.replace('SSE Error: ', ''));
		} catch {

		}
	}

	logger.error('Error from Anthropic', {
		message: err.message,
		status: err.status,
		type: err.type,
		details: err.details,
		subError: err.error,
	});

	ws.send(
		JSON.stringify({
			type: 'response-error',
			value: err,
		}),
	);
}

async function createMessages(
	promptArgs: PromptMessage,
): Promise<Array<Anthropic.MessageParam>> {
	const { prompt, repoState, repoPath, repoIgnoreList } = promptArgs;
	const historyMessages = promptArgs.messages || [];
	const messages: Array<Anthropic.MessageParam> = [];

	if (repoState === 'experimental' && repoPath) {
		const experimentalStateMessage = await getExperimentalRepoStateMessage(
			repoPath,
			repoIgnoreList,
		);
		messages.push({
			role: 'user',
			content: experimentalStateMessage,
		});
		messages.push({
			role: 'assistant',
			content: `Got it - I will refer back to this auto-generated message you have provided each time I form an answer.`,
		});
	} else if (repoState !== 'none' && repoPath) {
		const state = await getRepoState(repoState, repoPath, repoIgnoreList);
		const code = state
			.map((file) => `=== ${file.filePath} ===\n${file.contents}`)
			.join('\n\n');
		messages.push({
			role: 'user',
			content: `This is an automatically generated message.
It will be updated on every future chat message with the current state of the repository.
When I apply changes, the code will be updated in this message, even if we have continued chatting.
Please refer back to this message for every future prompt I send you, to get the correct state of the repository.

${code}`,
		});
		messages.push({
			role: 'assistant',
			content: `Got it - I will refer back to this code you have provided each time I form an answer.`,
		});
	}
	historyMessages.forEach((message) => {
		const { user, assistant } = message;
		messages.push({ role: 'user', content: user });
		messages.push({ role: 'assistant', content: assistant });
	});
	messages.push({
		role: 'user',
		content: prompt,
	});

	return messages;
}
