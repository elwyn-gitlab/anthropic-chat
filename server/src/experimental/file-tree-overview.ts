import { readFile } from 'fs/promises';
import { extname, resolve } from 'path';
import * as ts from 'typescript';
import { getFullRepoFileList } from '../repo-state';
import { logger } from '../logger';

export interface FileTreeOverviewItem {
	filePath: string;
	exportedSymbols: Array<string>;
}

interface TreeNode {
	name: string;
	files: Array<{
		name: string;
		exports: Array<string>;
	}>;
	children: Map<string, TreeNode>;
}

function createTreeNode(name: string): TreeNode {
	return {
		name,
		files: [],
		children: new Map(),
	};
}

function buildFileTree(files: Array<FileTreeOverviewItem>): TreeNode {
	const root = createTreeNode('');

	for (const file of files) {
		const parts = file.filePath.split('/');
		const fileName = parts.pop()!;
		let current = root;

		// Build directory structure
		for (const part of parts) {
			if (!current.children.has(part)) {
				current.children.set(part, createTreeNode(part));
			}
			current = current.children.get(part)!;
		}

		// Add file to current directory
		current.files.push({
			name: fileName,
			exports: file.exportedSymbols,
		});
	}

	return root;
}

function formatTreeNode(node: TreeNode, depth: number = 0): Array<string> {
	const lines: Array<string> = [];
	const indent = '  '.repeat(depth);

	// Handle current directory
	if (node.name) {
		lines.push(`${indent}/${node.name}`);
	}

	// Group files in current directory
	if (node.files.length > 0) {
		const filesList = node.files
			.map(f => {
				const exportsStr = f.exports.length > 0 ? `[${f.exports.join(',')}]` : '';
				return f.exports.length > 0 ? `${f.name}${exportsStr}` : f.name;
			})
			.join(', ');

		if (filesList) {
			lines.push(`${indent}  ${filesList}`);
		}
	}

	// Process subdirectories
	for (const child of node.children.values()) {
		lines.push(...formatTreeNode(child, depth + 1));
	}

	return lines;
}

function getExportedSymbols(sourceFile: ts.SourceFile): Array<string> {
	const symbols: Array<string> = [];

	function visit(node: ts.Node) {
		// Handle export declarations
		if (ts.isExportDeclaration(node)) {
			const { exportClause } = node;
			if (exportClause && ts.isNamedExports(exportClause)) {
				exportClause.elements.forEach(element => {
					symbols.push(element.name.text);
				});
			}
		}
		// Handle exported variables, functions, classes, etc
		else if (
			(ts.isVariableStatement(node) ||
				ts.isFunctionDeclaration(node) ||
				ts.isClassDeclaration(node) ||
				ts.isInterfaceDeclaration(node) ||
				ts.isTypeAliasDeclaration(node)) &&
			node.modifiers?.some(m => m.kind === ts.SyntaxKind.ExportKeyword)
		) {
			const name = (node as any).name;
			if (name) {
				symbols.push(name.text);
			}
		}
		// Handle default exports
		else if (ts.isExportAssignment(node)) {
			// Get the actual expression being exported
			const expression = node.expression;
			let defaultName = 'default';

			if (ts.isIdentifier(expression)) {
				// For: export default SomeIdentifier
				defaultName = `default:${expression.text}`;
			} else if (ts.isObjectLiteralExpression(expression)) {
				// For: export default { ... }
				defaultName = 'default:object';
			} else if (ts.isFunctionExpression(expression) || ts.isArrowFunction(expression)) {
				// For: export default function() { ... } or export default () => { ... }
				defaultName = 'default:function';
			} else if (ts.isClassExpression(expression)) {
				// For: export default class { ... }
				defaultName = 'default:class';
			}

			symbols.push(defaultName);
		}

		ts.forEachChild(node, visit);
	}

	visit(sourceFile);
	return symbols;
}

export async function getRepositoryFileTreeOverview(
	repoPath: string,
	ignoreList: Array<string>
): Promise<string> {
	const files = getFullRepoFileList(repoPath, ignoreList);
	const results: Array<FileTreeOverviewItem> = [];

	for (const filePath of files) {
		try {
			const fullPath = resolve(repoPath, filePath);
			const ext = extname(filePath).slice(1).toLowerCase();

			if (!['ts', 'js'].includes(ext)) {
				results.push({
					filePath,
					exportedSymbols: [],
				});
				continue;
			}

			const contents = await readFile(fullPath, 'utf-8');
			const sourceFile = ts.createSourceFile(
				filePath,
				contents,
				ts.ScriptTarget.Latest,
				true
			);

			results.push({
				filePath,
				exportedSymbols: getExportedSymbols(sourceFile),
			});
		} catch (error) {
			logger.error({ error, filePath }, 'Error parsing file');
		}
	}

	const tree = buildFileTree(results);
	const lines = formatTreeNode(tree);

	return `## Repo file tree\n${lines.join('\n')}`;
}


