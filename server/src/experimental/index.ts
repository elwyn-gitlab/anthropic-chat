import { getRepositoryFileTreeOverview } from './file-tree-overview';
import { getRepositoryTools } from './repo-tooling';
import { getRepositorySummary } from './repo-summary';
import { logger } from '../logger';

export async function getExperimentalRepoStateMessage(repoPath: string, repoIgnoreList: string): Promise<string> {
	const ignoreList = repoIgnoreList.split(',').map((item) => item.trim());

	const repoStateMessage = `Important instructions:
This is a generated message which contains important information about the project I am working on.
It will be updated on every future chat message with the current state of the repository.
When I apply changes, the code will be updated in this message, even if we have continued chatting (this message will be edited).
Please refer back to this message for every future prompt I send you, to get the correct state of the repository.

${await getRepositorySummary(repoPath, ignoreList)}

${await getRepoTooling(repoPath, ignoreList)}

${await getRepoFileTree(repoPath, ignoreList)}`;

	logger.debug('getExperimentalRepoStateMessage:\n', repoStateMessage, '\n\n');

	return repoStateMessage;
}

async function getRepoFileTree(repoPath: string, ignoreList: Array<string>): Promise<string> {
	const repoFileTree = await getRepositoryFileTreeOverview(repoPath, ignoreList);
	if (!repoFileTree.length) {
		return '';
	}

	return `## Repo file tree
These are the files in this repo, including the [exported symbols] of each.
${repoFileTree}`;
}

async function getRepoTooling(repoPath: string, ignoreList: Array<string>): Promise<string> {
	const toolConfigSummary = await getRepositoryTools(repoPath, ignoreList);
	if (!toolConfigSummary.length) {
		return '';
	}

	return `## Repo tooling
The following tool config files have been discovered. Be sure to take them into account when responding. For example, if a Prettier config file is included below, format your generated code accordingly.

${toolConfigSummary}`;
}


