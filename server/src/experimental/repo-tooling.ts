import { createHash } from 'crypto';
import { client, MESSAGE_BASE } from '../anthropic';
import { FileState, getFileContents, getFullRepoFileList } from '../repo-state';
import { logger } from '../logger';

interface CachedTools {
	hash: string;
	data: string;
}

const toolsCache = new Map<string, CachedTools>();

function getConfigHash(files: Array<string>): string {
	const hash = createHash('sha1');
	files.forEach(file => hash.update(file));
	return hash.digest('hex');
}

export async function getRepositoryTools(
	repoPath: string,
	ignoreList: Array<string>,
): Promise<string> {
	const files = getFullRepoFileList(repoPath, ignoreList);
	const configHash = getConfigHash(files);
	const cacheKey = `${repoPath}:${ignoreList.join(',')}`;
	const cached = toolsCache.get(cacheKey);

	if (cached && cached.hash === configHash) {
		logger.debug('Using cached repository tooling summary.');
		return cached.data;
	}

	logger.debug('Repository tooling summary not found, generating...');

	const message = await client.messages.create({
		...MESSAGE_BASE,
		messages: [
			{
				role: 'user',
				content: `You are helping build context about a repository which will be used in future chats.
You will be given a list of file paths. Your goal is to identify which files are related to project tooling, which might be useful when generating code in this repository.
For example, .prettierrc indicates the project uses Prettier for code formatting, so include that. An ESLint config will be useful to know which ESLint rules should be followed when generating code.
Not all tooling is necessarily relevant though. For example, a vite.config.ts, since using Vite does not really change the way we might generate future code.

package.json will be handled separately, do not include that.

If a file path is a match, include it in your response. Your response should be the list of file paths, in a valid JSON array. Do not include any other text in your response. If no files match, return an empty array.

${files.join('\n')}
`,
			},
		],
	});

	try {
		const toolFilePaths = JSON.parse(message.content?.at(0)?.text);
		if (!toolFilePaths.length) {
			return '';
		}

		const files = await getFileContents(repoPath, toolFilePaths);
		const result = await getToolsSummary(files);

		toolsCache.set(cacheKey, {
			hash: configHash,
			data: result,
		});

		return result;
	} catch (error) {
		logger.error(
			{ error, content: message.content?.at(0)?.text },
			`Failed to parse repository tools response.`,
		);
	}

	return '';
}

async function getToolsSummary(configFiles: Array<FileState>): Promise<string> {
	try {
		// TODO actual secret redaction
		const message = await client.messages.create({
			...MESSAGE_BASE,
			messages: [
				{
					role: 'user',
					content: `You are helping build context about a repository which will be used in future chats.
You will be given one or more tooling configuration files. Your goal is to generate a short summary of relevant configuration options, which can help ensure any code generated fits the project.

Use the following response structure:
<example-response>
./path/to/config.ts
relevant-config-excerp

./other-config.json
relevant-config-excerp
</example-response>

Pick out only parts of the config which will impact on the way that code is generated.
Do not include info related to repository structure, as that will be provided separately (e.g. tsconfig.json "source" is not needed).
If you see any secrets or tokens or other sensitive data, do not include it.

<example>
<example-input>
./tailwind.config.js
/** @type {import('tailwindcss').Config} */
export default {
    content: ["./client/index.html", "./client/src/**/*.{vue,js,ts}"],
    darkMode: true,
    theme: {
        extend: {},
    },
    variants: {
        extend: {},
    },
    plugins: [
        require("@tailwindcss/typography"),
        require("daisyui")
    ],
    daisyui: {
        themes: ['sunset'],
        prefix: "",
        logs: false,
    },
};
</example-input>
<example-output>
./tailwind.config.js
- Core Tailwind
- DaisyUI (sunset theme)
- Typography plugin
</example-output>
<example-explanation>
Parts of the config which would impact on generated code (such as available plugins) are summarized. The rest (content, darkMode, empty extend sections) are build/config related and don't affect HTML class usage.
</example-explanation>
</example>

Your response will be used directly. Do not include any additional text, only include the config data as specified above.

${configFiles.map((file) => `${file.filePath}\n${file.contents}`).join('\n\n')}
`,
				},
			],
		});

		return message.content?.at(0)?.text;
	} catch (error) {
		logger.error({ error }, `Failed to get repo tool summary.`);
	}

	return '';
}
