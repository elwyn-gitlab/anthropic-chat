import { BaseToolDefinition, BaseToolInput, ToolResult } from './';
import { execSync } from 'child_process';
import { logger } from '../../logger';
import { ensureRipgrepInstalled, getRipGrepPath } from './ripgrep';

interface CodeSearchInput extends BaseToolInput {
	pattern: string;
	filePattern?: string;
	contextLines?: number;
}
export const codeSearchTool: BaseToolDefinition<CodeSearchInput> = {
	name: 'search_code',
	description: `Search the codebase using ripgrep. Returns matching code snippets with context.

PATTERN SYNTAX:
- Basic text search: Simply provide the text to find, e.g. pattern='someFunction'
- Regular expressions are supported:
  - Word boundaries: pattern='\\bword\\b'
  - Character classes: pattern='Icon[A-Z][a-zA-Z]+'
  - Case insensitive: pattern='(?i)somefunction'
  - Pattern modifiers: pattern='(?-u)pattern'  (disable Unicode)

FILE FILTERING:
- Optionally filter by file glob: filePattern='*.{ts,js}'
- Multiple patterns: filePattern='{src,tests}/**/*.ts'

CONTEXT:
- contextLines: Number of lines to show before/after matches (default: 2)

Examples:
- Simple text: pattern='someFunction'
- Word boundaries: pattern='\\bIcon[A-Z][a-zA-Z]+\\b'
- Case insensitive: pattern='(?i)somefunction'
- Specific files: pattern='function' filePattern='*.ts'`,
	input_schema: {
		type: 'object',
		properties: {
			pattern: {
				type: 'string',
				description: 'Search pattern (literal text or regular expression)'
			},
			filePattern: {
				type: 'string',
				description: 'Optional file glob pattern to filter results'
			},
			contextLines: {
				type: 'number',
				description: 'Number of context lines to show before/after matches'
			}
		},
		required: ['pattern']
	},
	async execute(input: CodeSearchInput): Promise<ToolResult> {
		logger.debug('[Tools/codeSearchTool] executed. Input: ', input);
		const { repoIgnoreList } = input;

		await ensureRipgrepInstalled();

		const args = [
			getRipGrepPath(),
			'--no-heading',
			'--with-filename',
			'--line-number',
			`--context=${input.contextLines || 2}`,
			...repoIgnoreList.map(pattern => ['--glob', `'!${pattern}'`]).flat(), // Add ignore patterns - wrap patterns in quotes
			...input.filePattern ? ['-g', `'${input.filePattern}'`] : [], // Add include pattern if specified
			`'${input.pattern}'`, // Wrap pattern in quotes to avoid shell escaping issues
		];

		try {
			logger.debug('[Tools/codeSearchTool] Executing ripgrep: ', args.join(' '));

			const results = execSync(args.join(' '), {
				cwd: input.repoPath,
				encoding: 'utf-8',
				stdio: ['inherit', 'pipe', 'pipe']
			});

			logger.debug('[Tools/codeSearchTool] ripgrep results: ', results);

			return [{
				type: 'text',
				text: results || 'No matches found'
			}];
		} catch (error) {
			// Log the full error details
			logger.error('[Tools/codeSearchTool] ripgrep execution error\n', error.message, '\n', {
				error,
				stdout: error.stdout,
				stderr: error.stderr,
				status: error.status
			});

			// If we have stdout despite error, return it
			if (error.stdout) {
				return [{
					type: 'text',
					text: error.stdout
				}];
			}

			// ripgrep exits with code 1 when no matches found
			if (error.status === 1 && !error.stderr) {
				logger.debug('[Tools/codeSearchTool] ripgrep found no matches');
				return [{
					type: 'text',
					text: 'No matches found'
				}];
			}

			// Otherwise it's a real error
			return [{
				type: 'text',
				text: `Error executing search: ${error.stderr || error.message}`
			}];
		}
	}
};
