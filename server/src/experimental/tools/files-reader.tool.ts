// server/src/tools/file-reader.ts
import { BaseToolDefinition, BaseToolInput, ToolResult } from './';
import { getFileContents } from '../../repo-state';
import { logger } from '../../logger';

interface FileReaderInput extends BaseToolInput {
	paths: Array<string>;
}

export const filesReaderTool: BaseToolDefinition<FileReaderInput> = {
	name: 'read_files',
	description: `Reads the contents of specified files from the repository. The full file contents will be returned, along with the input path. This is the current state of the files and may overlap with results you see from the working_changes tool.
Input is an array of file paths relative to the repository root.
Files matching the repository ignore list will be excluded.
Binary files will be indicated but contents excluded.
Always use the read_files tool to examine file contents before making claims about implementation details or file contents.`,
	input_schema: {
		type: 'object',
		properties: {
			paths: {
				type: 'array',
				items: { type: 'string' },
				description: 'Array of file paths to read',
			},
		},
		required: ['paths'],
	},
	async execute(input: FileReaderInput): Promise<ToolResult> {
		logger.debug('[Tools/filesReaderTool] executed. Input: ', input);
		const files = await getFileContents(input.repoPath, input.paths);

		const formattedFiles = files.map(file =>
			`=== ${file.filePath} ===\n${file.contents}`
		).join('\n\n');

		return [{
			type: 'text',
			text: formattedFiles
		}];
	}
};
