import Anthropic from '@anthropic-ai/sdk';

export { toolRegistry } from './registry';

export interface BaseToolInput {
	repoPath: string;
	repoIgnoreList: Array<string>;
}

export interface ToolExecutor<TInput extends BaseToolInput, TOutput> {
	execute(input: TInput): Promise<TOutput>;
}

export type ToolResult = {
	type: 'text';
	text: string;
}[];

export type ToolPropertyType = {
	type: string;
	description?: string;
	items?: {
		type: string;
	};
};

export interface BaseToolDefinition<TInput extends BaseToolInput> extends Anthropic.Tool {
	input_schema: {
		type: 'object';
		properties: Record<string, ToolPropertyType>;
		required?: string[];
	};
	execute(input: TInput): Promise<ToolResult>;
}
