import { BaseToolDefinition, BaseToolInput } from './index';
import { filesReaderTool } from './files-reader.tool';
import { workingChangesTool } from './working-changes.tool';
import { codeSearchTool } from './code-search.tool';

export class ToolRegistry {
	private tools: Map<string, BaseToolDefinition<BaseToolInput>> = new Map();

	constructor() {
		[
			filesReaderTool,
			workingChangesTool,
			codeSearchTool,
		].forEach(tool => this.registerTool(tool));
	}

	registerTool<TInput extends BaseToolInput>(tool: BaseToolDefinition<TInput>) {
		this.tools.set(tool.name, tool as BaseToolDefinition<BaseToolInput>);
	}

	getTool(name: string): BaseToolDefinition<BaseToolInput> | undefined {
		return this.tools.get(name);
	}

	getRegisteredTools(): Array<BaseToolDefinition<BaseToolInput>> {
		return Array.from(this.tools.values());
	}
}

export const toolRegistry = new ToolRegistry();
