import { dirname, join } from 'path';
import { existsSync } from 'fs';
import { mkdir } from 'fs/promises';
import { execSync } from 'child_process';
import { logger } from '../../logger';

/* ripgrep downloading script
 * Roughly based on https://github.com/lvce-editor/ripgrep/blob/main/src/downloadRipGrep.js which unfortunately didn't run with Bun
* */

const REPOSITORY = `microsoft/ripgrep-prebuilt`;
const BIN_PATH = join(process.cwd(), 'node_modules/.bin');
const RIP_GREP_BINARY_PATH = join(BIN_PATH, 'rg');

function findSystemRipgrep(): string | null {
	try {
		const systemPath = execSync('which rg', { encoding: 'utf-8' }).trim();
		if (systemPath) {
			return systemPath;
		}
	} catch (error) {
		// no-op
	}
	return null;
}

// Export a getter to ensure we always return the current path
export const getRipGrepPath = () => findSystemRipgrep() || RIP_GREP_BINARY_PATH;

async function getLatestVersion(): Promise<string> {
	try {
		const response = await fetch(`https://api.github.com/repos/${REPOSITORY}/releases/latest`);
		if (!response.ok) {
			throw new Error(`GitHub API error: ${response.statusText}`);
		}
		const data = await response.json();
		return data.tag_name;
	} catch (error) {
		logger.error({ error }, 'Failed to get latest ripgrep version');
		throw new Error('Failed to get latest ripgrep version');
	}
}

function getTarget() {
	const arch = process.arch;
	const platform = process.platform;

	switch (platform) {
		case 'darwin':
			return arch === 'arm64' ? 'aarch64-apple-darwin.tar.gz' : 'x86_64-apple-darwin.tar.gz';
		case 'win32':
			return arch === 'x64' ? 'x86_64-pc-windows-msvc.zip' : 'i686-pc-windows-msvc.zip';
		case 'linux':
			return arch === 'x64' ? 'x86_64-unknown-linux-musl.tar.gz' : 'i686-unknown-linux-musl.tar.gz';
		default:
			throw new Error(`Unsupported platform: ${platform}`);
	}
}

async function downloadFile(url: string, outFile: string) {
	const response = await fetch(url);
	if (!response.ok) {
		throw new Error(`Failed to download: ${response.statusText}`);
	}

	await mkdir(dirname(outFile), { recursive: true });
	const blob = await response.blob();
	await Bun.write(outFile, blob);
}

async function extractArchive(inFile: string, outDir: string) {
	await mkdir(outDir, { recursive: true });

	if (inFile.endsWith('.tar.gz')) {
		execSync(`tar xf ${inFile} -C ${outDir}`);
	} else if (inFile.endsWith('.zip')) {
		// TODO: Implement zip extraction
		// Could use Bun.unzip once it's stable, or a pure JS implementation
		throw new Error('ZIP extraction not yet implemented');
	}
}

export async function ensureRipgrepInstalled(): Promise<void> {
	// First check for system ripgrep
	if (findSystemRipgrep()) {
		logger.debug(`Found system ripgrep in path`);
		return;
	}

	logger.debug(`System ripgrep was not in path`);

	// Then check our downloaded binary
	if (existsSync(RIP_GREP_BINARY_PATH)) {
		logger.debug(`Ripgrep binary detected at ${RIP_GREP_BINARY_PATH}`);
		return;
	}

	logger.info(`Ripgrep binary was not detected at ${RIP_GREP_BINARY_PATH}`);
	logger.info('Downloading ripgrep binary...');

	try {
		const version = await getLatestVersion();
		const target = getTarget();
		const url = `https://github.com/${REPOSITORY}/releases/download/${version}/ripgrep-${version}-${target}`;
		const downloadPath = `/tmp/ripgrep-${version}-${target}`;

		await downloadFile(url, downloadPath);
		await extractArchive(downloadPath, BIN_PATH);

		// Ensure the binary is executable
		execSync(`chmod +x ${RIP_GREP_BINARY_PATH}`);

		logger.info('Ripgrep binary downloaded successfully');
	} catch (error) {
		logger.error({ error }, 'Failed to download ripgrep binary');
		throw new Error('Failed to download ripgrep binary');
	}
}
