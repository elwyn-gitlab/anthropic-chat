import { BaseToolDefinition, BaseToolInput, ToolResult } from './';
import { execSync } from 'child_process';
import { logger } from '../../logger';

interface WorkingChangesInput extends BaseToolInput {
	repoPath: string;
	staged?: boolean;
}

export const workingChangesTool: BaseToolDefinition<WorkingChangesInput> = {
	name: 'get_working_changes',
	description: `Shows diff of current working changes in the repository.
If staged=true, shows only staged changes. If false or omitted, shows only unstaged changes.
Always use this tool if you do not know the current state of changes, especially when iterating on code or understanding my work.`,
	input_schema: {
		type: 'object',
		properties: {
			staged: {
				type: 'boolean',
				description: 'Whether to show staged (true) or unstaged (false) changes',
			},
		},
		required: [],
	},
	async execute(input: WorkingChangesInput): Promise<ToolResult> {
		logger.debug('[Tools/workingChangesTool] executed. Input: ', input);

		const gitCommand = input.staged
			? 'git diff --cached'
			: 'git diff';

		try {
			const diff = execSync(gitCommand, {
				cwd: input.repoPath,
				encoding: 'utf-8',
			});

			return [{
				type: 'text',
				text: diff || 'No changes found'
			}];
		} catch (error) {
			logger.error({ error }, 'Error getting working changes');
			throw new Error('Failed to get working changes');
		}
	}
};
