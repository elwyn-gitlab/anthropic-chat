import viteConfig from '../../../vite.config';

export async function devProxyHandler(req: Request): Promise<Response> {
	const url = new URL(req.url);
	const { port, host } = viteConfig.server;
	url.port = port.toString();
	url.hostname = host as string;
	return fetch(url.toString(), {
		headers: req.headers,
		method: req.method,
		body: req.body,
	});
}
