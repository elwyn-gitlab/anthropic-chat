export function optionsHandler(req: Request): Response {
	return new Response('Departed', {
		headers: {
			'Access-Control-Allow-Origin': '*',
			'Access-Control-Allow-Methods': 'OPTIONS, POST',
			'Access-Control-Allow-Headers': '*',
		},
	});
}
