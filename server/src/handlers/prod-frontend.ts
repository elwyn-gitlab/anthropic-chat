import { join } from 'path';
import { existsSync } from 'fs';

const clientDir = existsSync(join(process.cwd(), './dist/client'))
	? './dist/client'
	: './client';

export function prodFrontendHandler(req: Request): Response {
	const url = new URL(req.url);
	const favicon = '/icon.png';
	if (url.pathname === favicon) {
		const assetPath = join(clientDir, favicon);
		return new Response(Bun.file(assetPath));
	} else if (url.pathname.startsWith('/assets/')) {
		const assetPath = join(clientDir, url.pathname.slice(1));
		return new Response(Bun.file(assetPath));
	} else {
		return new Response(Bun.file(join(clientDir, 'index.html')), {
			headers: { 'Content-Type': 'text/html' },
		});
	}
}
