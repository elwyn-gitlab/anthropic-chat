import { ServerWebSocket } from 'bun';

import { getRepoState } from '../repo-state';

export async function repoStatePreviewHandler(
	ws: ServerWebSocket<unknown>,
	repoState: 'changes' | 'full',
	repoPath = process.env.REPO_PATH,
	repoIgnoreList: string,
) {
	const state = await getRepoState(repoState, repoPath, repoIgnoreList);
	ws.send(
		JSON.stringify({
			type: 'repo-state-preview',
			value: state,
		}),
	);
}
