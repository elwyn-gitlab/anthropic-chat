export function websocketUpgradeHandler(
	req: Request,
	server: any,
): Response | undefined {
	const url = new URL(req.url);
	if (url.pathname === '/chat') {
		const upgraded = server.upgrade(req);
		if (!upgraded) {
			return new Response('Upgrade failed', { status: 400 });
		}
	}
}
