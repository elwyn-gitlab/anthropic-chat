import { serve } from 'bun';

import { runPrompt } from './anthropic';
import { optionsHandler } from './handlers/options';
import { prodFrontendHandler } from './handlers/prod-frontend';
import { websocketUpgradeHandler } from './handlers/socket-upgrade';
import { repoStatePreviewHandler } from './handlers/repo-state-preview';
import { PromptMessage } from '../../client/src/ws';
import { logger } from './logger';

const isDev = process.env.NODE_ENV !== 'production';

const port = 3939;
const hostname = 'localhost';

export interface MessageWrapper<T> {
	type: string;
	value: T;
}

const server = serve({
	port,
	hostname,
	websocket: {
		open: (ws) => logger.info('Client connected'),
		close: (ws) => logger.info('Client disconnected'),
		message: async (ws, msg) => {
			if (typeof msg !== 'string') {
				logger.error({ msg }, 'Received unexpected message');
				return;
			}

			const message = JSON.parse(msg) as MessageWrapper<unknown>;
			switch (message.type) {
				case 'prompt':
					await runPrompt(message.value as PromptMessage, ws);
					break;
				case 'get-repo-state-preview':
					const { repoState, repoPath, repoIgnoreList } =
						message.value as {
							repoState: 'changes' | 'full';
							repoPath: string;
							repoIgnoreList: string;
						};
					await repoStatePreviewHandler(
						ws,
						repoState,
						repoPath,
						repoIgnoreList,
					);
					break;
				default:
					logger.error(
						{ type: message.type },
						'Received unknown message type:',
					);
			}
		},
	},
	async fetch(req, server) {
		if (req.method === 'OPTIONS') {
			return optionsHandler(req);
		}

		websocketUpgradeHandler(req, server);

		if (isDev) {
			if (process.env.npm_lifecycle_event.includes('bundle')) {
				// TODO: resolve issue where for `bundle` script to run, this needs to be hardcoded `false` for dev scripts to be correctly tree shaken out
				// Otherwise the Vite dev server stuff ends up in the bundle and throws errors.
				throw new Error(
					'Sorry, you forgot to hardcode this isDev check to false when bundling.',
				);
			}

			const devProxyHandlerModule = await import(
				'./handlers/dev-frontend-proxy'
			);
			return devProxyHandlerModule.devProxyHandler(req);
		}

		return prodFrontendHandler(req);
	},
});

logger.info(`
             ____                             ______
|         | |            |        |         .~      ~.
|_________| |______      |        |        |          |
|         | |            |        |        |          |
|         | |___________ |_______ |_______  \`.______.'

Server is listening on http://${hostname}:${port}

`);

process.on('exit', () => {
	server.stop();
	logger.info('Server stopped.');
});
