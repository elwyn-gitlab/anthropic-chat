type LogLevel = 'debug' | 'info' | 'warn' | 'error';

const isDev = process.env.NODE_ENV !== 'production';
const logLevel: LogLevel = isDev ? 'debug' : 'info';

function getPrefix() {
	return `[${new Date().toISOString()}]`;
}

function shouldLog(level: LogLevel): boolean {
	const levels: Array<LogLevel> = ['debug', 'info', 'warn', 'error'];
	return levels.indexOf(level) >= levels.indexOf(logLevel);
}

export const logger = {
	debug: (...args: Array<any>) => {
		if (shouldLog('debug')) {
			console.debug(getPrefix(), '[DEBUG]', ...args);
		}
	},
	info: (...args: Array<any>) => {
		if (shouldLog('info')) {
			console.info(getPrefix(), '[INFO]', ...args);
		}
	},
	warn: (...args: Array<any>) => {
		if (shouldLog('warn')) {
			console.warn(getPrefix(), '[WARN]', ...args);
		}
	},
	error: (...args: Array<any>) => {
		if (shouldLog('error')) {
			console.error(getPrefix(), '[ERROR]', ...args);
		}
	},
};
