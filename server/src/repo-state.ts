import { execSync } from 'child_process';
import { readFileSync } from 'fs';
import { normalize, join } from 'path';
import { minimatch } from 'minimatch';

import { logger } from './logger';

export interface FileState {
	filePath: string;
	contents: string;
}

const BINARY_FILE_CONTENT_PLACEHOLDER =
	'// binary or non-text file, contents excluded';

export async function getRepoState(
	type: 'changes' | 'full' | 'experimental',
	cwd = process.env.REPO_PATH,
	repoIgnoreList: string,
): Promise<FileState[]> {
	if (!cwd) {
		throw new Error(
			'Failed to get repo state because cwd could not be determined.',
		);
	}

	const ignoreList = repoIgnoreList.split(',').map((item) => item.trim());

	if (type === 'full') {
		const filePaths = getFullRepoFileList(cwd, ignoreList);
		return await getFileContents(cwd, filePaths);
	}

	if (type === 'changes') {
		return getChangedRepoFileList(cwd, ignoreList);
	}

	logger.warn('Repo state not found.');
	return [];
}

export function getFullRepoFileList(cwd: string, ignoreList: Array<string>) {
	const gitCommand =
		'git ls-files && git ls-files --others --exclude-standard';

	return getFileList(gitCommand, cwd, ignoreList);
}

export async function getChangedRepoFileList(
	cwd: string,
	ignoreList: Array<string>,
): Promise<Array<FileState>> {
	const defaultBranch = getDefaultBranch(cwd);
	logger.debug(`Detected default branch for repo: "${defaultBranch}"`);

	const committedChanges = getCommittedChanges(
		cwd,
		defaultBranch,
		ignoreList,
	);
	const uncommittedChanges = getUncommittedChanges(cwd, ignoreList);

	const dedup = <T>(arr: Array<T>): Array<T> => Array.from(new Set(arr));
	const allChangedFiles = [...committedChanges, ...uncommittedChanges].filter(
		(filePath) => !isFileExcluded(filePath, ignoreList),
	);

	return getFileContents(cwd, dedup(allChangedFiles));
}

function getCommittedChanges(
	cwd: string,
	defaultBranch: string,
	ignoreList: Array<string>,
): Array<string> {
	const gitCommand = `git diff --name-only --diff-filter=ACMR $(git merge-base HEAD origin/${defaultBranch})`;
	const changedFiles = execSync(gitCommand, { cwd, encoding: 'utf-8' })
		.trim()
		.split('\n');

	logger.debug(
		'getCommittedChanges\n' + JSON.stringify(changedFiles, null, 4),
	);
	return changedFiles.filter(Boolean);
}

function getUncommittedChanges(
	cwd: string,
	ignoreList: Array<string>,
): Array<string> {
	const stagedFiles = execSync('git diff --name-only --cached', {
		cwd,
		encoding: 'utf-8',
	})
		.trim()
		.split('\n');
	const unstagedFiles = execSync(
		'git ls-files --modified --others --exclude-standard',
		{ cwd, encoding: 'utf-8' },
	)
		.trim()
		.split('\n');
	const changedFiles = new Set([...stagedFiles, ...unstagedFiles]);

	logger.debug(
		'getUncommittedChanges\n' +
			JSON.stringify({ stagedFiles, unstagedFiles }, null, 4),
	);
	return Array.from(changedFiles).filter(Boolean);
}

function getDefaultBranch(cwd: string): string {
	try {
		const remoteBranch = execSync(
			'git rev-parse --abbrev-ref origin/HEAD',
			{ cwd, encoding: 'utf-8' },
		).trim();
		return remoteBranch.replace('origin/', '');
	} catch (error) {
		const branches = ['origin/main', 'origin/master'];
		for (const branch of branches) {
			try {
				execSync(`git rev-parse --verify ${branch}`, {
					cwd,
					stdio: 'ignore',
				});
				return branch;
			} catch (error) {
				// Branch doesn't exist, continue to the next one
			}
		}
		throw new Error(
			'Failed to determine default branch for this repository.',
		);
	}
}

function getFileList(
	gitCommand: string,
	cwd: string,
	ignoreList: Array<string>,
): Array<string> {
	const gitOutput = execSync(gitCommand, { cwd, encoding: 'utf-8' });

	const fileList = Array.from(new Set(gitOutput.trim().split('\n')));

	return applyIgnoreList(fileList, ignoreList);
}

function applyIgnoreList(files: Array<string>, ignoreList: Array<string>) {
	return files.filter((filePath) => !isFileExcluded(filePath, ignoreList));
}

function isFileExcluded(filePath: string, ignoreList: Array<string>): boolean {
	for (const pattern of ignoreList) {
		if (pattern.startsWith('!')) {
			if (minimatch(filePath, pattern.slice(1))) {
				return false;
			}
		} else {
			if (minimatch(filePath, pattern)) {
				return true;
			}
		}
	}
	return false;
}

export async function getFileContents(
	cwd: string,
	filePaths: Array<string>,
): Promise<Array<FileState>> {
	return filePaths.map((filePath) => {
		const normalizedPath = normalize(filePath);
		const fullPath = join(cwd, normalizedPath);

		let contents = BINARY_FILE_CONTENT_PLACEHOLDER;

		try {
			const fileBuffer = readFileSync(fullPath);
			if (isTextFile(fileBuffer)) {
				contents = fileBuffer.toString('utf-8');
			}
		} catch (error) {
			logger.error({ error, fullPath }, 'Error reading file');
			contents = `// Error reading file: ${error.message}`;
		}

		return {
			filePath,
			contents,
		};
	});
}

function isTextFile(buffer: Buffer): boolean {
	const textEncoding = 'utf-8';
	const textChars = buffer.toString(textEncoding);
	const binaryChars = buffer.toString('binary');
	return textChars.length === binaryChars.length;
}
