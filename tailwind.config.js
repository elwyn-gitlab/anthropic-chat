/** @type {import('tailwindcss').Config} */
export default {
	content: ["./client/index.html", "./client/src/**/*.{vue,js,ts}"],
	darkMode: true,
	theme: {
		extend: {},
	},
	variants: {
		extend: {},
	},
	plugins: [
		require("@tailwindcss/typography"),
		require("daisyui")
	],
	daisyui: {
		themes: ['sunset'],
		prefix: "",
		logs: false,
	},
};
