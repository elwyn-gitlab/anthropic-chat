import type { UserConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { visualizer } from "rollup-plugin-visualizer";
import checker from "vite-plugin-checker";

const viteConfig: UserConfig = {
	root: "./client",
	plugins: [
		vue(),
		visualizer({
			filename: "./dist/bundle-stats.html",
		}),
		checker({
			overlay: false,
			terminal: true,
			vueTsc: true,
		}),
	],
	server: {
		port: 5151,
		host: "localhost",
		hmr: {
			clientPort: 5151,
		},
	},
	build: {
		outDir: "../dist/client",
		emptyOutDir: true,
		sourcemap: true,
	},
};

export default viteConfig;
